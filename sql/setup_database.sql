#
# This script initalizes an empty MySQL database with the tables, views, etc.
# that are required for this application.
#
# WARNING:  Since the SQL-statements inside this file will be executed, if the
#           database-setup-lock-file does not exist (and this may a
#           configuration mistake), the statements should be as passive as
#           possible and try to avoid to break existing data.
#

CREATE TABLE IF NOT EXISTS `parcel_status` (
    `Parcel` VARCHAR(32) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NOT NULL,
    `Time` TIMESTAMP NOT NULL,
    `Status` TINYTEXT CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NOT NULL,
    `CurrentLocation` TEXT CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NOT NULL,
    `Destination` TEXT CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NOT NULL,

    INDEX USING BTREE (`Parcel`)
);
