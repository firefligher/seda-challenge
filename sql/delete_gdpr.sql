#
# This script deletes all rows which reference the specified parcel-id from the
# parcel_status table.
#
# Parameters:
#  __parcel:            VARCHAR(32)
#

DELETE FROM
    `parcel_status`

WHERE
    `parcel_status`.`Parcel` = :__parcel
