#
# This script inserts a row into the parcel_status table.
#
# Parameters:
#  __parcel:            VARCHAR(32)
#  __time:              TIMESTAMP
#  __status:            TINYTEXT(255)
#  __currentLocation:   TEXT(65535)
#  __destination:       TEXT(65535)
#

INSERT INTO `parcel_status` (
    `Parcel`,
    `Time`,
    `Status`,
    `CurrentLocation`,
    `Destination`
) VALUES (
    :__parcel,
    :__time,
    :__status,
    :__currentLocation,
    :__destination
);
