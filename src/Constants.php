<?php

namespace SedaChallenge;

/**
 * A collection of constants whose values are required at different locations
 * in the application.
 */
class Constants {
    #
    # ************************** DEPENDENCY INJECTION *************************
    #
    # The key for each object that is available via dependency injection should
    # exist as a DI_* constant to avoid duplicates and simplify renaming
    # (uniqueness of the value is required here).

    public const DI_CONFIGURATION   = "configuration";
    public const DI_DATABASE        = "database";
    public const DI_CRYPTO          = "crypto";
    public const DI_CODECS          = "codec_registry";

    #
    # ***************************** SQL STATEMENTS ****************************
    #
    # The name of each file inside the "./sql" directory (relative from inside
    # the root directory of the repository) without ".sql" suffix.
    
    public const STMT_DELETE_GDPR       = "delete_gdpr";
    public const STMT_INSERT_STATUS     = "insert_status";
    public const STMT_SETUP_DATABASE    = "setup_database";
    
    #
    # ************************ SQL STATEMENT PARAMETERS ***********************
    #
    # The names of the placeholders that have been used in the SQL-scripts. The
    # naming scheme of the constant is as follows:
    #
    # P_<script-name>_<placeholder-without-suffix>
    #
    #
    # Statement: STMT_DELETE_GDPR

    public const P_DELETE_GDPR_PARCEL = ":__parcel";

    # Statement: STMT_INSERT_STATUS

    public const P_INSERT_STATUS_PARCEL             = ":__parcel";
    public const P_INSERT_STATUS_TIME               = ":__time";
    public const P_INSERT_STATUS_STATUS             = ":__status";
    public const P_INSERT_STATUS_CURRENT_LOCATION   = ":__currentLocation";
    public const P_INSERT_STATUS_DESTINATION        = ":__destination";
}
