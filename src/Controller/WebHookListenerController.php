<?php

namespace SedaChallenge\Controller;

use Psr\Container\ContainerInterface;
use Psr\Http\Message\ResponseInterface as Response;
use Psr\Http\Message\ServerRequestInterface as Request;
use SedaChallenge\Constants;
use SedaChallenge\Model\Event;

/**
 * The web-hook endpoint handler.
 */
class WebHookListenerController {
    /**
     * The header that the requester is using for specifying the signature of
     * the payload.
     */
    private const HEADER_X_WEBHOOK_SIGNATURE    = "X-Webhook-Signature";

    /**
     * The standard HTTP header that is used for specifing the content type of
     * the payload.
     */
    private const CONTENT_TYPE                  = "Content-Type";

    /**
     * The content type that is expected to be present, if the payload contains
     * JSON-encoded data structures. 
     */
    private const MIME_APPLICATION_JSON         = "application/json";

    /**
     * The crypto service instance.
     */
    private $crypto;

    /**
     * The database service instance.
     */
    private $database;

    /**
     * The codec registry instance.
     */
    private $codecs;

    /**
     * If this flag is `true`, the controller will validate the signature of
     * the payload that has been received.
     */
    private $requestVerificationEnabled;

    public function __construct(ContainerInterface $container) {
        $this->crypto = $container->get(Constants::DI_CRYPTO);
        $this->database = $container->get(Constants::DI_DATABASE);
        $this->codecs = $container->get(Constants::DI_CODECS);

        # Getting the "verify_requests" flag from the configuration

        $config = $container->get(Constants::DI_CONFIGURATION);
        $this->requestVerificationEnabled =
            $config["security"]["verify_requests"];
    }

    public function __invoke(
        Request $request,
        Response $response,
        array $args): Response {
        # Validating that the request has the right Content-Type and that the
        # signature header is present

        if ($request->getHeaderLine(self::CONTENT_TYPE)
            !== self::MIME_APPLICATION_JSON) {
            return $response->withStatus(400, "Bad Request");
        }

        if (!$request->hasHeader(self::HEADER_X_WEBHOOK_SIGNATURE)) {
            return $response->withStatus(400, "Bad Request");
        }

        $signature = hex2bin($request->getHeaderLine(
            self::HEADER_X_WEBHOOK_SIGNATURE));

        # Retrieving the request body and validating the signature

        $requestBody = (string) $request->getBody();

        if ($this->requestVerificationEnabled &&
            !$this->crypto->validateSignature($requestBody, $signature)) {
            return $response->withStatus(403, "Forbidden");
        }

        # Decoding and deserializing the JSON payload

        $bodyArray = json_decode($requestBody, TRUE);

        if (!$bodyArray) {
            return $response->withStatus(400, "Bad Request");
        }

        $event = $this->codecs->decode($bodyArray, Event::class);

        # Depending on the specified event, we need to handle the request
        # diffently.

        switch ($event->getEvent()) {
        case "parcel.status.updated":
            # TODO: NULL-checks should be performed

            $parcel = $event->getParcel();

            $this->database->insert(Constants::STMT_INSERT_STATUS, array(
                Constants::P_INSERT_STATUS_TIME => $event->getOccurredOn(),
                Constants::P_INSERT_STATUS_PARCEL => $parcel->getId(),
                Constants::P_INSERT_STATUS_STATUS => $parcel->getStatus(),
                Constants::P_INSERT_STATUS_CURRENT_LOCATION =>
                    $parcel->getCurrentLocation(),
                    
                Constants::P_INSERT_STATUS_DESTINATION =>
                    $parcel->getDestination()
            ));
            break;

        case "parcel.deleted":
            # TODO: NULL-checks should be performed too

            $parcel = $event->getParcel();

            $this->database->delete(Constants::STMT_DELETE_GDPR, array(
                Constants::P_DELETE_GDPR_PARCEL => $parcel->getId()
            ));
            break;

        default:
            return $response->withStatus(400, "Bad Request");
        }

        return $response;
    }
}
