<?php

namespace SedaChallenge\Model;

class Event {
    private $event;
    private $occurredOn;
    private $parcel;

    public function __construct(
        string $event,
        string $occurredOn,
        Parcel $parcel) {
        $this->event = $event;
        $this->occurredOn = $occurredOn;
        $this->parcel = $parcel;
    }

    public function getEvent(): string {
        return $this->event;
    }

    public function getOccurredOn(): string {
        return $this->occurredOn;
    }

    public function getParcel(): Parcel {
        return $this->parcel;
    }
}
