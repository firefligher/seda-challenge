<?php

namespace SedaChallenge\Model;

class Parcel {
    private $id;
    private $status;
    private $currentLocation;
    private $destination;

    public function __construct(
        string $id,
        ?string $status,
        ?string $currentLocation,
        ?string $destination) {
        $this->id = $id;
        $this->status = $status;
        $this->currentLocation = $currentLocation;
        $this->destination = $destination;
    }

    public function getId(): string {
        return $this->id;
    }

    public function getStatus(): ?string {
        return $this->status;
    }

    public function getCurrentLocation(): ?string {
        return $this->currentLocation;
    }

    public function getDestination(): ?string {
        return $this->destination;
    }
}
