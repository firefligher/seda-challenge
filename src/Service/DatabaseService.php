<?php

namespace SedaChallenge\Service;

use \SedaChallenge\Constants;

/**
 * The service that establishes and manages the connection to the database.
 */
class DatabaseService {
    /**
     * The base directory of the SQL-script directory.
     */
    private const SQL_BASE_DIR_PATH = __DIR__."/../../sql";

    /**
     * The handle of the established database connection.
     */
    private $pdoHandle;

    /**
     * Creates a new instance of the database service and establishes the
     * database connection.
     * 
     * If the file at "setup_lock_path" does not exist, the database service
     * will attempt to initialize the specified database.
     * 
     * @param   array   $dbConfig   The "database" section of the
     *                              configuration.php
     */
    public function __construct(array $dbConfig) {
        # Establishing a connection to the database

        $this->pdoHandle = new \PDO(
            $dbConfig["dsn"],
            $dbConfig["user"],
            $dbConfig["password"],
            $dbConfig["options"]);

        # TODO: Exception handling.
        #
        # Testing, if the database-setup-lock-file exists. If not, we may
        # attempt to initialize the database, if the corresponding flag has
        # been enabled.

        $lockPath = $dbConfig["setup_lock_path"];

        if (!file_exists($lockPath) && $dbConfig["setup_empty"]) {
            # Initializing the database structure and creating the lock-file.

            $this->bulkUpdate(Constants::STMT_SETUP_DATABASE);

            if (!touch($lockPath)) {
                throw new \Exception("Cannot create database setup lock!");
            }
        }
    }

    /**
     * Executes the specified SQL-statement as it is (without any parameters).
     * 
     * If the statement-file contains multiple statements (separated by the
     * configured separator), this method will attempt to execute all of them.
     * 
     * @param   string  $stmtName   The name of the statement that shall be
     *                              executed.
     */
    public function bulkUpdate(string $stmtName): void {
        # NOTE: Also if we are going to use PDO's query-operation, which does
        #       not prevent from potential SQL-injections, we should be safe
        #       here, because we execute predefined SQL-statements without
        #       allowing dynamic parameters.
        #
        # TODO: Error handling, if query fails.

        $this->pdoHandle->query($this->getStatement($stmtName));
    }

    /**
     * Executes a statement that performs an insertion-operation and returns
     * the last inserted id (only useful, if AUTO_INCREMENT flag was enabled
     * for the PRIMARY_KEY column).
     * 
     * @param   string  $stmtName   The name of the statement that shall be
     *                              executed.
     * 
     * @param   array   $params     An array that specifies the values
     *                              (array-value) of the script's placeholders
     *                              (array-key).
     * 
     * @return  int The id of the last inserted column. The return value is
     *              undefined, if the updated table does not provide a
     *              PRIMARY_KEY with enabled AUTO_INCREMENT flag.
     */
    public function insert(string $stmtName, array $params): int {
        $stmt = $this->prepare($stmtName, $params);

        if (!$stmt->execute()) {
            throw new \Exception("Failed to execute statement: $stmtName!");
        }

        return $this->pdoHandle->lastInsertId();
    }

    /**
     * Executes a statement that performs a deletion-operation and returns
     * whether one or multiple rows have been removed (due to the operation)
     * or not.
     * 
     * @param   string  $stmtName   The name of the statement that shall be
     *                              executed.
     * 
     * @param   array   $params     An array that specifies the values
     *                              (array-value) of the script's placeholders
     *                              (array-key).
     * 
     * @return  bool    Whether one or multiple rows have been removed due to
     *                  this operation or not.
     */
    public function delete(string $stmtName, array $params): bool {
        $stmt = $this->prepare($stmtName, $params);

        if (!$stmt->execute()) {
            throw new \Exception("Failed to execute statement: $stmtName!");
        }

        return $stmt->rowCount() > 0;
    }

    /**
     * Prepares a new instance of the specified statement and configures its
     * placeholders with the passed `$params`.
     * 
     * @param   string  $stmtName   The name of the statement that shall be
     *                              prepared.
     * 
     * @param   array   $params     An array that specifies the values
     *                              (array-value) of the script's placeholders
     *                              (array-key).
     * 
     * @return  \PDOStatement   The prepared and configured statement.
     */
    private function prepare(string $stmtName, array $params): \PDOStatement {
        $stmt = $this->pdoHandle->prepare($this->getStatement($stmtName));

        # Binding the placeholders of the prepared statement their passed
        # values. This also escapes the values to avoid SQL-injections.

        foreach (array_keys($params) as $placeholder) {
            # Since bindParam's second parameter is passed by-reference, we
            # need to ensure that we do not the passed variable in the next
            # iteration of the loop.
            #
            # Thus, we cannot iterate with foreach($params as $key => $value),
            # because value would become reused within each iteration.

            $stmt->bindParam($placeholder, $params[$placeholder]);
        }

        return $stmt;
    }

    /**
     * Resolves the content of the SQL-script with the specified `$stmtName`.
     * 
     * @param   string  $stmtName   The name of the SQL-script that shall be
     *                              resolved.
     * 
     * @return  string  The content of the resolved statement.
     */
    private function getStatement(string $stmtName): string {
        $filePath = sprintf("%s/%s.sql", self::SQL_BASE_DIR_PATH, $stmtName);

        if (!file_exists($filePath)) {
            throw new \Exception("Could not find file of SQL statement!");
        }

        # TODO: Handle IO errors

        return file_get_contents($filePath);
    }
}
