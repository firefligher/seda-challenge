<?php

namespace SedaChallenge\Service;

/**
 * An implementation of the required cryptography utilities.
 */
class CryptoService {
    /**
     * The public key that is being used for validating signatures.
     */
    private $publicKey;

    /**
     * The hash algorithm that is being used for validating signatures.
     */
    private $hashAlgorithm;

    /**
     * Creates a new instance.
     * 
     * @param   array   $securityConfig The "security" section of the
     *                                  configuration.php
     */
    public function __construct(array $securityConfig) {
        # Either the public key has been specified directly inside the
        # configuration file, or its path has been specified.

        $keyData = $securityConfig["public_key"];

        if ($keyData === NULL) {
            # TODO: Handle IO errors.

            $keyData = file_get_contents(
                $securityConfig["public_key_path"]);
        }

        $this->publicKey = openssl_pkey_get_public($keyData);
        $this->hashAlgorithm = $securityConfig["sig_hash_algorithm"];
    }

    /**
     * Validates that the `$signature` of the specified `$message` has been
     * created by the owner of the configured public/private-key-pair.
     * 
     * @param   string  $message    The raw message whose signature shall be
     *                              validated.
     * 
     * @param   string  $signature  The signature that shall be validated.
     * 
     * @return  bool    Either `true`, if the signature is valid and has been
     *                  created by somebody who has access to the private-key
     *                  for the configured public-key, otherwise `false`.
     */
    public function validateSignature(
        string $message,
        string $signature): bool {
        $b64Message = base64_encode($message);

        return openssl_verify(
            $b64Message, $signature,
            $this->publicKey,
            $this->hashAlgorithm);
    }
}
