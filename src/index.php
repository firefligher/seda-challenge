<?php

namespace SedaChallenge;

use DI\Container;
use SedaChallenge\Codec\CodecRegistry;
use SedaChallenge\Service\CryptoService;
use SedaChallenge\Service\DatabaseService;
use Slim\Factory\AppFactory;

require_once __DIR__."/../vendor/autoload.php";

#
# Setting up the services
#

$container = new Container();
$container->set(Constants::DI_CONFIGURATION, function () {
    # Including the configuration file function-locally to avoid conflicts with
    # existing $config variables of the global scope.
    
    require_once __DIR__."/../configuration.php";

    # The $config array is defined inside the global scope of the included
    # file.

    return $config;
});

$container->set(Constants::DI_DATABASE, function (Container $container) {
    $config = $container->get("configuration");
    return new DatabaseService($config["database"]);
});

$container->set(Constants::DI_CRYPTO, function (Container $container) {
    $config = $container->get("configuration");
    return new CryptoService($config["security"]);
});

$container->set(Constants::DI_CODECS, function (Container $container) {
    return new CodecRegistry();
});

#
# Setting up the application and registering the endpoints
#

AppFactory::setContainer($container);

$app = AppFactory::create();
$config = $container->get("configuration");

if ($config["development"]) {
    # If the execution of the application takes place inside a development
    # environment, we want to receive all issues as detailed as possible.

    $app->addErrorMiddleware(TRUE, TRUE, TRUE);
} else {
    # If we're in a productive environment, we do not want to display the
    # stacktrace to the user as it may contain sensitve information (like
    # database credentials), but we still want to log everything.

    $app->addErrorMiddleware(FALSE, TRUE, TRUE);
}

$app->post(
    "/webhooklistener",
    Controller\WebHookListenerController::class);

# Execution

$app->run();
