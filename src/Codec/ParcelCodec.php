<?php

namespace SedaChallenge\Codec;

use \SedaChallenge\Model\Parcel;

/**
 * The serialization codec for the `\SedaChallenge\Model\Parcel` class.
 */
class ParcelCodec implements Codec {
    public function decode(
        array $serialized,
        CodecRegistry $registry): object {
        $id = NULL;
        $status = NULL;
        $currentLocation = NULL;
        $destination = NULL;

        # We always need a parcel identifier

        if (!array_key_exists("id", $serialized)) {
            throw new \Exception("No parcel id specified!");
        }

        $id = $serialized["id"];

        # The remaining properties may be optional, e.g. if the GDPR web hook
        # is being called.

        if (array_key_exists("status", $serialized)) {
            $status = $serialized["status"];
        }

        if (array_key_exists("current_location", $serialized)) {
            $currentLocation = $serialized["current_location"];
        }

        if (array_key_exists("destination", $serialized)) {
            $destination = $serialized["destination"];
        }

        return new Parcel($id, $status, $currentLocation, $destination);
    }
}

