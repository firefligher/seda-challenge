<?php

namespace SedaChallenge\Codec;

/**
 * A codec that implements the deserialization of a specific model class from
 * an `array`.
 */
interface Codec {
    /**
     * Deserializes the `$serialized` array to an instance of the codec's
     * specific model class.
     * 
     * @param   array           $serialized The serialized data of the model.
     * @param   CodecRegistry   $registry   The registry of all codecs (may be
     *                                      used for deserializing
     *                                      sub-objects).
     */
    public function decode(array $serialized, CodecRegistry $registry): object;
}
