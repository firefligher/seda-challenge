<?php

namespace SedaChallenge\Codec;

use \SedaChallenge\Model\Event;
use \SedaChallenge\Model\Parcel;

/**
 * The serialization codec for the `\SedaChallenge\Model\Event` class.
 */
class EventCodec implements Codec {
    public function decode(
        array $serialized,
        CodecRegistry $registry): object {
        # TODO: Validating that the referenced keys are existing.

        $event = $serialized["event"];
        $occurredOn = $serialized["occurred_on"];
        $parcel = $registry->decode($serialized["parcel"], Parcel::class);

        return new Event($event, $occurredOn, $parcel);
    }
}
