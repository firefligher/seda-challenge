<?php

namespace SedaChallenge\Codec;

use \SedaChallenge\Model\Event;
use \SedaChallenge\Model\Parcel;

/**
 * The central management unit for the serialization of objects.
 */
class CodecRegistry {
    /**
     * An array of all registered codecs.
     * 
     * The key of each entry is the name of the model that can be serialized by
     * using the instance of a codec implementation, which is the value of the
     * corresponding entry.
     */
    private $registeredCodecs;

    /**
     * Creates a new instance and registers the models/initializes the codec
     * instances.
     */
    public function __construct() {
        $this->registeredCodecs = array();

        $this->register(Event::class, new EventCodec());
        $this->register(Parcel::class, new ParcelCodec());
    }

    /**
     * Attempts to decode the object as instance of the `$targetModel` from the
     * `$serialized` data.
     * 
     * @param   array   $serialized     The data that shall be deserialized.
     * @param   string  $targetModel    The class of the instance that shall be
     *                                  deserialized from the passed data.
     * 
     * @return  object  The decoded instance.
     */
    public function decode(array $serialized, string $targetModel): object {
        if (!array_key_exists($targetModel, $this->registeredCodecs)) {
            throw new \Exception("No codec for model!");
        }

        $codec = $this->registeredCodecs[$targetModel];
        return $codec->decode($serialized, $this);
    }

    /**
     * Registers a `$modelClass` and its corresponding `$codecInstance`.
     * 
     * @param   string  $modelClass     The class of the model.
     * @param   Codec   $codecInstance  The instance of the corresponding codec
     *                                  that is capable of deserializing the
     *                                  model from an `array`.
     */
    private function register(string $modelClass, Codec $codecInstance): void {
        if (array_key_exists($modelClass, $this->registeredCodecs)) {
            throw new \Exception("Class already registered!");
        }

        $this->registeredCodecs[$modelClass] = $codecInstance;
    }
}
