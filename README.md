# SEDA Challenge

This project is a solution for the [SEDA.digital PHP Coding Challenge](https://www.notion.so/SEDA-digital-PHP-Coding-Challenge-6d04abcd9389430ea6943d95c50d1af7).

## Setup / Environment

All possible setups require that you provide a (empty) MySQL database. The
DBMS needs to be accessible from the PHP interpreter. (Configure the firewall
settings of your host accordingly.)

You need to issue the following command once after cloning the repository:

```shell
composer install
```

### Configuration

The application can be configured via the `configuration.php` (inside the
repository root). The following table describes the different settings:

| Property Path               | Data Type | Description                                                               |
|:--------------------------- |:---------:|:------------------------------------------------------------------------- |
| development                 | bool      | If enabled, the application will present detailed stacktraces to the user (if an error occurs). |
| security.verify_request     | bool      | If enabled, the application will validate the signatures of all requests to the web-hook endpoint. |
| security.public_key         | ?string   | The public key that will be used for validating the request signatures. If `NULL`, a non-null value is required for *security.public_key_path*. |
| security.public_key_path    | ?string   | The path to the public key file whose key will be used for validating the request signature. If `NULL`, a non-null value is required for *security.public_key*. |
| security.sig_hash_algorithm | mixed     | The hash algorithm that will be used for verifying the signatures of requests. |
| database.dsn                | string    | The Data Source Name of the target database ([Manual](https://www.php.net/manual/de/pdo.construct.php)). |
| database.user               | string    | The user that will be used for authenticating at the DBMS. |
| database.password           | string    | The password that will be used for authenticating at the DBMS. |
| database.options            | ?array    | Additional driver-specific options ([Manual](https://www.php.net/manual/de/pdo.construct.php)). |
| database.setup_empty        | bool      | If enabled, the application will attempt to initialize the specified database, if the file at *database.setup_lock_path* does not exist. |
| database.setup_lock_path    | string    | The path to the database initialization lock-file. (Have a look at *database.setup_empty*.) |

### PHP-CLI built-in web server

This is the simplest way to run this application. If PHP is in your `PATH`
variable, you can just open a command-line interpreter and navigate to the root
directory of this repository. Then, issuing the following command should be
enough to start the server:

```shell
php -S localhost:8080 ./src/index.php
```

Of course, you are free to choose another port and another interface to listen
on.

### Apache

The simplest way to get this application running, is to set up a virtual host
with the following information:

```
<VirtualHost 127.0.0.1:80>
    ServerName seda-challenge.local
    DocumentRoot "/path/to/the/repository/src"
</VirtualHost>
```

**Notice:** The `DocumentRoot` should not be the repository root, but the
src-directory inside the repository root. Also `AllowOverride` should be
enabled.

**Warning:** You may also need adjust Apache's and your file system's directory
access restrictions.
