<?php

#
# This file represents the configuration file of this application. You should
# adjust the values to match your environment.
#

$config = array();

#
# *********************************** GLOBAL **********************************
#

$config["development"] = TRUE;

#
# ********************************** SECURITY *********************************
#
# NOTE: Instead of specifying the public_key directly in this configuration
#       file, you may also assign a (absolute) file path to the key file to the
#       "public_key_path" property. (In this case, you should set "public_key"
#       to NULL.)

$config["security"] = array();
$config["security"]["verify_requests"] = TRUE;
$config["security"]["public_key"]      = <<<EOD
-----BEGIN PUBLIC KEY-----
MIIBIjANBgkqhkiG9w0BAQEFAAOCAQ8AMIIBCgKCAQEA1XsHnWVq1wyo3xSif9gB
aHfKK5HrqJtPoxhfyFzumJV6Bl4FRckEFPhI+0qQeQG+hQwThBc/7qzyK3oc1vop
M0CQ1X9wiFct2va+dozU4jyt4Duwsvgynq9pvvCSpVoW91UZALpjnUH3IF9QrMoE
4wpJMjh4QDhzgKlzWQh+AvVE4AX7HkmCIhutNJSzsQSO8I+/9Qo1LH67uVgMLRPz
eGX0xQLhOlPT4VSqRFO3mppybHphluXKskmnsXYnsbfEwlJWyFHln31691Lqri1Z
x2AwAyfK0thY+rCXqpAeQnIRxXpXiUF1KaOjI/MWe6vQRvAg3tU8tlz3o7dhstgb
uQIDAQAB
-----END PUBLIC KEY-----
EOD;

$config["security"]["public_key_path"]      = NULL;
$config["security"]["sig_hash_algorithm"]   = OPENSSL_ALGO_SHA1;

#
# ********************************** DATABASE *********************************
#
# NOTE: PDO will be used for establishing a connection to the database and
#       performing queries. For further information, have a look at
#       https://www.php.net/manual/de/pdo.construct.php.
#
# WARNING:  The database MUST exist already!

$config["database"] = array();
$config["database"]["dsn"]      = "mysql:dbname=seda_challenge;host=localhost;port=3306;charset=utf8mb4";
$config["database"]["user"]     = "root";
$config["database"]["password"] = "";
$config["database"]["options"]  = NULL;

# The following value will not be passed to the PDO constructor, but will be
# used for determining whether an empty database should be initialized properly
# or not.
#
# NOTE: Due to performance reasons, setting up a database will only take place,
#       if the file at "setup_lock_path" does not exist. Afterwards, it is
#       attempted to create the lock-file.

$config["database"]["setup_empty"]      = TRUE;
$config["database"]["setup_lock_path"]  = __DIR__."/database_setup.lock";
